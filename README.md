# Data Logger hw
**Main sensors and components :**  
1. STM32L431RCT6 (MCU)
2. W25Q128JVSIM (flash memory) 
3. SD card 
4. LSM9DSOTR (IMU)
5. BNO055 (IMU) (_will not be populated on the v1 of the board due to the global chip shortage_)
6. MS560702BA03-50 (Altimeter) (_the board includes 2 of these sensors_)
7. BMP384 (Altimeter)
8. NEO-M8J-0 (GPS)  

\*Altimeters are actually barometric pressure sensors

Unassembled Data Logger front view  :    

<img src="/images/DL_coin.jpg" alt="Data Logger size scale" title="DL v1.0.0 size scale" width = "400" align="center">

Unassembled Data Logger back view :   

<img src="/images/DL_back.jpg" alt="Data Logger back side" title="DL v1.0.0 back" width = "400">

**Libraries :**  
Besides a project specific library that is included in this repo, 
one should download LSF Kicad library (https://gitlab.com/librespacefoundation/lsf-kicad-lib.git) 
and set LSF_KICAD_LIB as an environment variable in their system.  


